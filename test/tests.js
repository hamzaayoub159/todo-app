const chai = require("chai")
const chaiHttp = require("chai-http")
const should = chai.should()
const log = require("../App/Config/logger")
const app = require("../app")
chai.use(chaiHttp)
//User Test Cases
describe("Users", () => {
  it("should add a SINGLE user on /users POST", done => {
    chai
      .request(app)
      .post("/users")
      .send({
        userId: 123,
        username: "hamza",
        email: "hamza159@yahoo.com",
        password: "1234567"
      })
      .end((err, result) => {
        result.should.have.status(200)
        console.log(" Body ", result.body)
      })
    done()
  })
  it("should login a user using email and password", done => {
    const login = {
      email: "hamza159@yahoo.com",
      password: "1234567"
    }
    chai
      .request(app)
      .post("/users/login")
      .send(login)
      .end((err, result) => {
        result.should.have.status(200)
        console.log(" Body ", result.body)
      })
    done()
  })
})

//Test Cases for TODO CRUD operations
describe("Todos CRUD", () => {
  const todo = {
    _id: "5daf17fdeb43b6589f5d1fdc",
    description: "this is my first todo",
    completed: false
  }
  it("should add a SINGLE todo on /todos POST", done => {
    chai
      .request(app)
      .post("/todos")
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200)
        console.log(" Body ", result.body)
      })

    done()
  })

  it("should list all todos on /todos/GET", done => {
    chai
      .request(app)
      .get("/todos")
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200)
        log.info("Got", result.body.data.lenght, "docs")
      })
    done()
  })
  it("should list a SINGLE todo on /todos/:_id GET", done => {
    chai
      .request(app)
      .get("/todos/" + todo._id)
      .send(todo)
      .end((err, result) => {
        result.should.have.status(200)
        console.log("Fetched Paricular todos using /GET/TODOS:_ID")
      })
    done()
  })
  it("should update a SINGLE todo on /todos/:_id PATCH", done => {
    chai
      .request(app)
      .patch("/todos/" + todo._id)
      .end((err, result) => {
        result.should.have.status(200)
        result.body.data.completed.should.eq(true)
        console.log("updated todo")
      })
    done()
  })
  it("should delete a SINGLE todo on /todos/:_id DELETE", done => {
    chai
      .request(app)
      .delete("/todos/" + todo._id)
      .end((err, result) => {
        result.should.have.status(200)
        console.log("TODO DELETED")
      })
    done()
  })
})
