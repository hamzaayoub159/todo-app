const express = require("express")
const bodyParser = require("body-parser")
const cors = require("cors")

const morgan = require("morgan")
const userRoute = require("./App/Routes/User")
const todoRoute = require("./App/Routes/Task")

const app = express()

// define middleware
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())
//routes
app.use(todoRoute)
app.use(userRoute)

//using morgan
app.use(morgan("dev"))
app.use("/", (req, res) => {
  res.status(200).send("testing123....")
})
module.exports = app
