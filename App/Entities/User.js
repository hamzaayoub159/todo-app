const uuid = require("uuid/v1")
class User {
  constructor(userId, userName, email, password) {
    this.userId = userId
    this.userName = userName
    this.email = email
    this.password = password
  }

  //Function will convert Plain Object into JSON object
  toObject() {
    return JSON.stringify(this)
  }

  //Function will create user object from given {User} object
  static createFromObject(userObj) {
    return new User(
      userObj.userId,
      userObj.userName,
      userObj.email,
      userObj.password
    )
  }

  //Function will create user object from given {User} detail
  static createFromDetails(userName, email, password) {
    return new User(uuid(), userName, email, password)
  }
}

module.exports = User
