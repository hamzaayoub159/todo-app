const uuid = require("uuid/v1")
class Todo {
  constructor(description, completed, userId) {
    this.description = description
    this.completed = completed
    this.userId = userId
  }

  //Function will convert Plain Object into JSON object
  toObject() {
    return JSON.stringify(this)
  }

  //Function will create todo object from given {todo} object
  static createFromObject(todoObj) {
    return new Todo(todoObj.description, todoObj.completed, todoObj.userId)
  }
  static createFromDetails(description, completed, userId) {
    return new Todo(description, completed, userId)
  }
}
module.exports = Todo
