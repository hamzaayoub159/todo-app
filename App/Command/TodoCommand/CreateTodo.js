const entity = require("../../Entities/Task")
const log = require("../../Config/logger")
const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus")
const taskService = require("../../Services/Task")

// CreateAccount Command
class CreateAccountCommand extends Command {
  constructor(description, completed, userId) {
    super()
    this.description = description
    this.completed = completed
    this.userId = userId
  }
}
// CreateAccount Handler
class CreateAccountHandler {
  async handle(command) {
    try {
      const result = await taskService.add(command)
      return result
    } catch (e) {
      throw new error()
    }
  }
}

// Handler middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ CreateAccountHandler: new CreateAccountHandler() }),
  new HandleInflector()
)

// Command bus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
])
const create = async (req, res) => {
  const { body } = req
  try {
    const result = await entity.createFromObject({
      ...body,
      userId: req.user.userId
    })
    console.log(result)
    const createAccountCommand = new CreateAccountCommand(
      result.description,
      result.completed,
      result.userId
    )
    const commandHandle = await commandBus.handle(createAccountCommand)

    res.send(commandHandle)
  } catch (e) {
    log.debug(e)
  }
}

module.exports = create
