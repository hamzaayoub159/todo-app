const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus")
const todoService = require("../../Services/Task")
const log = require("../../Config/logger")

class LoadAllTodoCommand extends Command {
  constructor(description, completed) {
    super()
    this.description = description
    this.completed = completed
  }
}

class LoadAllTodoHandler {
  async handle(command) {
    const data = await todoService.findAll(command)
    return data
  }
}
//Handler Middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ LoadAllTodoHandler: new LoadAllTodoHandler() }),
  new HandleInflector()
)

//Create CommandBus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
])

const loadAll = async (req, res) => {
  try {
    const loadAllTodoCommand = new LoadAllTodoCommand(
      this.description,
      this.completed
    )
    const result = await commandBus.handle(loadAllTodoCommand)
    res.status(200).send(result)
  } catch (e) {
    log.error(e)
  }
}

module.exports = loadAll
