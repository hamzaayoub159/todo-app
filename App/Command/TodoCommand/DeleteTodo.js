const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus");
const todoService = require("../../Services/Task");
const log = require("../../Config/logger");

class RemoveTodoCommand extends Command {
  constructor(_id) {
    super();
    this._id = _id;
  }
}

class RemoveTodoHandler {
  async handle(command) {
    const data = await todoService.remove(command._id);
    return data;
  }
}
//Handler Middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ RemoveTodoHandler: new RemoveTodoHandler() }),
  new HandleInflector()
);

//Create CommandBus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
]);

const remove = async (req, res) => {
  const { _id } = req.params;
  const removeTodoCommand = await new RemoveTodoCommand(_id);
  try {
    const result = await commandBus.handle(removeTodoCommand);
    if (!result) {
      res.status(404).send("Id not found");
    }
    res.status(200).send("Todo Remove Successfully");
  } catch (e) {
    log.error(e);
  }
};

module.exports = remove;
