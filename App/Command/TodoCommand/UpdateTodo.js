const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus")
const todoService = require("../../Services/Task")

// Update Command
class UpdateTodoCommand extends Command {
  constructor(_id, description, completed) {
    super()
    this._id = _id
    this.description = description
    this.completed = completed
  }
}

// Update Handler
class UpdateTodoHandler {
  async handle(command) {
    const updateData = await todoService.update(command._id, command)
    return updateData
  }
}
// Handler middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ UpdateTodoHandler: new UpdateTodoHandler() }),
  new HandleInflector()
)
// Command bus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
])
const update = async (req, res) => {
  try {
    const { _id } = req.params
    const { body } = req
    const updates = Object.keys(body)
    const allowUpdates = ["description", "completed"]
    const isValidOperation = updates.every(update =>
      allowUpdates.includes(update)
    )
    if (!isValidOperation) {
      res.status(400).send("Invalid Update")
    }
    const updateTodoCommand = new UpdateTodoCommand(
      _id,
      body.description,
      body.completed
    )
    const result = await commandBus.handle(updateTodoCommand)
    console.log(result)
    res.status(200).send(result)
  } catch (e) {
    res.send(e)
  }
}
module.exports = update
