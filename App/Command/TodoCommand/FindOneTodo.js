const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus")
const todoService = require("../../Services/Task")

class LoadTodoByIdCommand extends Command {
  constructor(_id) {
    super()
    this._id = _id
  }
}

class LoadTodoByIdHandler {
  async handle(command) {
    const data = await todoService.findById(command._id)
    return data
  }
}
//Handler Middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ LoadTodoByIdHandler: new LoadTodoByIdHandler() }),
  new HandleInflector()
)

//Create CommandBus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
])

const loadById = async (req, res) => {
  const { _id } = req.params
  const loadTodoByIdCommand = new LoadTodoByIdCommand(_id)
  try {
    const result = await commandBus.handle(loadTodoByIdCommand)
    res.status(200).send(result)
  } catch (e) {
    log.error(e)
  }
}

module.exports = loadById
