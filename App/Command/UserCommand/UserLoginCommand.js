const bcrypt = require("bcryptjs")
const userService = require("../../Services/User")
const log = require("../../Config/logger")
const jwt = require("jsonwebtoken")
const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus")

// CreateAccount Command
class LoginCommand extends Command {
  constructor(email) {
    super()
    this.email = email
  }
}

// CreateAccount Handler
class LoginHandler {
  async handle(command) {
    try {
      const result = await userService.findByEmail(command)
      return result
    } catch (e) {
      log.error(e)
    }
  }
}
// Handler middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ LoginHandler: new LoginHandler() }),
  new HandleInflector()
)

// Command bus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
])
const findByEmail = async (req, res) => {
  try {
    const email = req.body.email
    console.log(req.body.email)
    const password = req.body.password
    const loginCommand = new LoginCommand(email)
    console.log(loginCommand)
    const promise = await commandBus.handle(loginCommand)
    console.log(promise)

    const isMatch = await bcrypt.compare(password, promise.password)
    if (promise) {
      if (!isMatch) {
        res.status(400).send("Password does not match")
      } else {
        const token = jwt.sign(
          {
            userId: promise.userId
          },
          "thisismynewcourse"
        )

        res.send({ promise, token })
      }
    } else {
      res.status(400).send("Invalid Email")
    }
  } catch (e) {
    log.error(e)
  }
}

module.exports = findByEmail
