const {
  Command,
  CommandBus,
  CommandHandlerMiddleware,
  ClassNameExtractor,
  InMemoryLocator,
  HandleInflector,
  LoggerMiddleware
} = require("simple-command-bus");
const userService = require("../../Services/User");
const userEntities = require("../../Entities/User");
const logger = require("../../Config/logger");

// CreateAccount Command
class CreateAccountCommand extends Command {
  constructor(userName, email, password) {
    super();
    this.userName = userName;
    this.email = email;
    this.password = password;
  }
}

// CreateAccount Handler
class CreateAccountHandler {
  async handle(command) {
    const user = await userService.add(command);
    return user;
  }
}

// Handler middleware
const commandHandlerMiddleware = new CommandHandlerMiddleware(
  new ClassNameExtractor(),
  new InMemoryLocator({ CreateAccountHandler: new CreateAccountHandler() }),
  new HandleInflector()
);

// Command bus instance
const commandBus = new CommandBus([
  new LoggerMiddleware(console),
  commandHandlerMiddleware
]);
const create = async (req, res) => {
  try {
    const { body } = req;
    const userEntity = await userEntities.createFromObject(body);
    const createAccountCommand = await new CreateAccountCommand(
      userEntity.userName,
      userEntity.email,
      userEntity.password
    );
    const result = await commandBus.handle(createAccountCommand);
    console.log("result == ", result);
    const token = result.generateAuthToken();
    res.send({ result, token });
  } catch (e) {
    logger.debug(e);
  }
};

module.exports = create;
