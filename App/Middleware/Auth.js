let jwt = require("jsonwebtoken")
const User = require("../Model/User")
const auth = async (req, res, next) => {
  try {
    const token = req.header("x-auth-token")
    const decoded = jwt.verify(token, "thisismynewcourse")
    const user = await User.findOne({
      userId: decoded.userId
    })
    if (!user) {
      throw new Error()
    }
    req.user = user
    next()
  } catch (e) {
    res.status(401).send({ error: "Please authenticate." })
  }
}
module.exports = auth
