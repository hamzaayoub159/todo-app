const express = require("express")
const router = express.Router()
const command = require("../Command/TodoCommand/CreateTodo")
const findOne = require("../Command/TodoCommand/FindOneTodo")
const findAll = require("../Command/TodoCommand/FindAllTodos")
const deleteTodo = require("../Command/TodoCommand/DeleteTodo")
const update = require("../Command/TodoCommand/UpdateTodo")
const auth = require("../Middleware/Auth")
//save data
router.post("/todos", auth, command)

//get all data
router.get("/todos", auth, findAll)
//get data one by one through id
router.get("/todos/:_id", auth, findOne)
//get data and update
router.patch("/todos/:_id", auth, update)
//getdata and delete
router.delete("/todos/:_id", auth, deleteTodo)

module.exports = router
