const program = require("commander");
const log = require("../Config/logger");
const app = require("../../app");
const port = 3000;
//require mongoDB Database
require("../Config/DbConfig");
program.version("0.0.1").description("todo-app");

//Start Server
app.listen(port, () => {
  log.info("Server Starting on Port " + port);
});

program.parse(process.argv);
