const todoStore = require("../Store/Task")
const todoEntity = require("../Entities/Task")
const log = require("../Config/logger")
class TodoService {
  static async add(todoObj) {
    try {
      const { description, completed, userId } = todoObj
      const todo = todoEntity.createFromDetails(description, completed, userId)
      const todoStoreResult = await todoStore.add(todo)
      return todoStoreResult
    } catch (e) {
      log.debug(e)
    }
  }
  static async findAll() {
    try {
      const todoStoreResult = await todoStore.findAll()
      return todoStoreResult
    } catch (e) {
      log.debug(e)
    }
  }
  static async findById(todoId) {
    try {
      const todoStoreResult = await todoStore.findById(todoId)
      return todoStoreResult
    } catch (e) {
      log.debug(e)
    }
  }
  static async update(todoId, todoObj) {
    try {
      const todoStoreResult = await todoStore.update(todoId, todoObj)
      return todoStoreResult
    } catch (e) {
      log.debug(e)
    }
  }
  static async remove(todoId) {
    try {
      const todoStoreResult = await todoStore.remove(todoId)
      return todoStoreResult
    } catch (e) {
      log.debug(e)
    }
  }
}

module.exports = TodoService
