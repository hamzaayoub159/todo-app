const userStore = require("../Store/User")
const logger = require("../Config/logger")
const User = require("../Entities/User")

class UserService {
  static async add(userObj) {
    try {
      const { userName, email, password } = userObj
      const user = User.createFromDetails(userName, email, password)
      const userStoreResult = await userStore.add(user)
      return userStoreResult
    } catch (e) {
      logger.debug(e)
    }
  }
  static async findByEmail(email) {
    try {
      const userStoreResult = await userStore.findByEmail(email)
      return userStoreResult
    } catch (e) {
      logger.debug(e)
    }
  }
}

module.exports = UserService
