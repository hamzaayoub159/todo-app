const todoModel = require("../Model/Task")
const todoEntities = require("../Entities/Task")

class TodoStore {
  static async add(todoObj) {
    return await todoModel.create(todoObj)
  }
  static async findAll() {
    const todoModelResult = await todoModel.find()
    return todoModelResult
  }

  static async findById(_id) {
    const todoModelResult = await todoModel.findOne({ _id })
    return todoEntities.createFromObject(todoModelResult)
  }
  static async update(_id, todoObj) {
    const todoModelResult = await todoModel.findByIdAndUpdate(_id, todoObj)
    return todoEntities.createFromDetails(todoModelResult)
  }
  static async remove(_id) {
    const todoModelResult = await todoModel.findOneAndDelete({ _id })
    return todoEntities.createFromObject(todoModelResult)
  }
}
module.exports = TodoStore
