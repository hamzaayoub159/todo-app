const userModel = require("../Model/User")
const userEntity = require("../Entities/User")

class UserStore {
  static async add(userObj) {
    return await userModel.create(userObj)
  }

  static async findByEmail(email) {
    const userModelResult = await userModel.findOne(email)
    return userEntity.createFromObject(userModelResult)
  }
}
module.exports = UserStore
