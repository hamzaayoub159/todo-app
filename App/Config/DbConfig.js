const mongoose = require("mongoose")
const log = require("./logger")
mongoose.promise = global.promise
mongoose.connect("mongodb://localhost:27017/todoapp", {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false
})
log.info("Successfully Connected With MongoDB Database")
