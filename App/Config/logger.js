const bunyan = require("bunyan")
const log = bunyan.createLogger({
  name: "todoapp",
  streams: [
    {
      level: "info",
      stream: process.stdout
    },
    {
      level: "error",
      stream: process.stdout
    },
    {
      level: "debug",
      stream: process.stdout
    }
  ]
})
module.exports = log
